# [0.10.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.9.0...v0.10.0) (2022-11-21)



# [0.9.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.8.0...v0.9.0) (2021-05-19)



# [0.8.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.7.0...v0.8.0) (2020-10-20)


### Features

* add command to copy issues ([755a301](https://gitlab.com/openstapps/gitlab-api/commit/755a3013a6edb43f727baf21f081bc2251cbe3cd))



# [0.7.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.6.1...v0.7.0) (2020-05-06)


### Features

* add merge request assignment ([927dcb1](https://gitlab.com/openstapps/gitlab-api/commit/927dcb159b5a89be959443d642af85ef1cfc0020))



## [0.6.1](https://gitlab.com/openstapps/gitlab-api/compare/v0.6.0...v0.6.1) (2020-01-08)


### Bug Fixes

* use passed method instead of resetting it ([db56457](https://gitlab.com/openstapps/gitlab-api/commit/db5645732cca9bdf7f36b250d49273753aaa0126))



# [0.6.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.5.1...v0.6.0) (2019-11-19)


### Features

* add batch processing of issues ([fcf7cac](https://gitlab.com/openstapps/gitlab-api/commit/fcf7caca502e9c57c595998be001d57d79f8f207))
* add possibility to configure retrying of requests ([1796e88](https://gitlab.com/openstapps/gitlab-api/commit/1796e882271bceda9c2c9bebccb6847491ecd338))



## [0.5.1](https://gitlab.com/openstapps/gitlab-api/compare/v0.5.0...v0.5.1) (2019-02-25)


### Bug Fixes

* correct return type for method ([a74ad2f](https://gitlab.com/openstapps/gitlab-api/commit/a74ad2f6d3eae693e56385bc2d6e85d356f3ba95))



# [0.5.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.4.0...v0.5.0) (2019-02-25)


### Features

* add method to fetch merge request discussions ([4ef2cfb](https://gitlab.com/openstapps/gitlab-api/commit/4ef2cfb7382973487b7be03b714270ed40d0c7ae))
* add types for discussions ([7c9b56f](https://gitlab.com/openstapps/gitlab-api/commit/7c9b56f6784397bc142330f16c175acd8b941c91))



# [0.4.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.3.1...v0.4.0) (2019-02-05)


### Features

* add enums for stricter types ([2dc6e80](https://gitlab.com/openstapps/gitlab-api/commit/2dc6e808a944b66e66de3719477c4e1ed2995d7e))
* add type for merge request approval ([9d5e272](https://gitlab.com/openstapps/gitlab-api/commit/9d5e2727f82e3abb6680cdde6d1b8a8c1159e9c0))



## [0.3.1](https://gitlab.com/openstapps/gitlab-api/compare/v0.3.0...v0.3.1) (2019-01-28)


### Bug Fixes

* correct property order ([3e494ce](https://gitlab.com/openstapps/gitlab-api/commit/3e494ce0dcf1e45711b3b0891962acfd9954a61f))



# [0.3.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.2.3...v0.3.0) (2019-01-28)


### Features

* add method to get subgroups ([1854f38](https://gitlab.com/openstapps/gitlab-api/commit/1854f38190bd6d146b55c127a2294187653beb02))



## [0.2.3](https://gitlab.com/openstapps/gitlab-api/compare/v0.2.2...v0.2.3) (2019-01-07)



## [0.2.2](https://gitlab.com/openstapps/gitlab-api/compare/v0.2.1...v0.2.2) (2019-01-07)



## [0.2.1](https://gitlab.com/openstapps/gitlab-api/compare/v0.2.0...v0.2.1) (2019-01-07)


### Bug Fixes

* correctly delete labels ([5f36b53](https://gitlab.com/openstapps/gitlab-api/commit/5f36b5372f87d912c2b733971dca659203021d67))



# [0.2.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.1.3...v0.2.0) (2018-11-30)


### Bug Fixes

* correctly build request url ([ad5b0b9](https://gitlab.com/openstapps/gitlab-api/commit/ad5b0b9fe5487d1a217fda93295d7097394f1be8))



## [0.1.3](https://gitlab.com/openstapps/gitlab-api/compare/v0.1.2...v0.1.3) (2018-11-30)


### Bug Fixes

* remove undocumented default params ([5e358bc](https://gitlab.com/openstapps/gitlab-api/commit/5e358bcacdb182d8695da73d373dd9a1b7e34b15))



## [0.1.2](https://gitlab.com/openstapps/gitlab-api/compare/v0.1.1...v0.1.2) (2018-11-29)


### Bug Fixes

* use correct prefix for binary ([547e28f](https://gitlab.com/openstapps/gitlab-api/commit/547e28fc5742c90d5362f3a534cdd2e756ae8791))



## [0.1.1](https://gitlab.com/openstapps/gitlab-api/compare/v0.1.0...v0.1.1) (2018-11-29)



# [0.1.0](https://gitlab.com/openstapps/gitlab-api/compare/v0.0.1...v0.1.0) (2018-11-29)



## [0.0.1](https://gitlab.com/openstapps/gitlab-api/compare/54d212963cb6fb507f5275f18bbccb4e10496b1c...v0.0.1) (2018-11-29)


### Features

* add GitLab API ([54d2129](https://gitlab.com/openstapps/gitlab-api/commit/54d212963cb6fb507f5275f18bbccb4e10496b1c))



